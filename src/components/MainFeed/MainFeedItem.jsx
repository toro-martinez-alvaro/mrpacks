import React, { Component } from "react";

class MainFeedItem extends Component {
  render() {
    return (
      <div>
          
        <img target="_blank" src={this.props.image} alt="No se ha encontrado la ruta de la imagen" />>
        <div>{this.props.image}</div>
        <div>{this.props.description}</div>
        <div>{this.props.youtubeName}</div>
      </div>
    );
  }
}

export default MainFeedItem;
