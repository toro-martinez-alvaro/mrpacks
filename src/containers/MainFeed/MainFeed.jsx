import React, { Component } from "react";
import MainFeedItem from "../../components/MainFeed/MainFeedItem";

class MainFeed extends Component {
  state = {
    arrayItems: [],
    ocultar: false
  };

  

  crearItems = () => {
      const videos = [
        {
           image: "https://i.ytimg.com/vi/X2j0Sr_6wU4/hqdefault.jpg",
          description: <a href="https://www.youtube.com/watch?v=X2j0Sr_6wU4" >"Buscando a marika"</a>,
          youtubeName: <a target="_blank" href="https://www.youtube.com/">"Youtube.com"</a> 
        },
        {
          image: "https://i.ytimg.com/vi/YatlMG-Kgck/hqdefault.jpg",
          description: <a href="https://www.youtube.com/watch?v=YatlMG-Kgck" >¡vamos marika, animo!</a>,
          youtubeName: <a target="_blank" href="https://www.youtube.com/">"Youtube.com"</a> 
        },
        {
          image: "https://i.ytimg.com/vi/3YdstBteGHM/maxresdefault.jpg",
          description: <a target="_blank" href="https://www.youtube.com/watch?v=3YdstBteGHM" >PODNITO</a>,
          youtubeName: <a target="_blank" href="https://www.youtube.com/">"Youtube.com"</a> 
        },
        {
          image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvuhjFJzcnTGg5oZOSmy-39w12PDtyR8SJCP5HnFWyR30SZl0e",
          description: <a target="_blank" href="https://www.muycerdas.xxx/chica-ucraniana-se-frota-se-mete-varios-dedos-cono/" >"peppa pig"</a>,
          youtubeName: <a target="_blank" href="https://www.youtube.com/">"Youtube.com"</a> 
        },
        {
          image: "Imagen Del primer video",
          description: <a target="_blank" href="" >video</a>,
          youtubeName: <a target="_blank" href="https://www.youtube.com/">"Youtube.com"</a> 
        },
        {
          image: "Imagen Del primer video",
          description: <a target="_blank" href="" >video</a>,
          youtubeName: <a target="_blank" href="https://www.youtube.com/">"Youtube.com"</a> 
        }]

      this.setState({
          arrayItems: videos
      })
  };

  ocultarItems = () => {
    this.setState({
        ocultar: true 
    })
  }

  render() {
    return (
      <div>
        <button onClick={this.crearItems}>Mostrar contenido</button>
        <button onClick={this.ocultarItems}>Ocultar contenido</button>
        {!this.state.ocultar && this.state.arrayItems.map(video => {
          return (
            <MainFeedItem
              image={video.image}
              description={video.description}
              youtubeName={video.youtubeName}
            />
          );
        })}
      </div>
    );
  }
}

export default MainFeed;
