import React, {Component} from "react";
import Category from "../../components/menu lateral/Category";

class MenuLateral extends Component {
    render(){
        return(
            <div className="menuLateral">
                <ul>
                    <Category name = "opcion #1" items={["1", "2","3"]}/>
                    <Category name = "opcion #2" items={["1", "2","3"]}/>
                    <Category name = "opcion #3" items={["1", "2","3"]}/>
                    <Category name = "opcion #4" items={["1", "2","3"]}/>
                    <Category name = "opcion #5" items={["1", "2","3"]}/>
                    <Category name = "Contactanos" items={["Telefonos", "Correo electronico"]}/>
                </ul>
            </div>
        );
    }
}

export default MenuLateral;