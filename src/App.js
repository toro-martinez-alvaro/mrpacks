import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MainFeed from './containers/MainFeed/MainFeed';
import NavBar from "./components/searchBar/NavBar.jsx";
import MenuLateral from "./containers/menu lateral/MenuLateral";




class App extends Component {
  render() {
    return (
      <div className="App">
         <NavBar/> {/* Barra de navegacion */}
         <MainFeed image="Soy una imagen"></MainFeed>
         <MenuLateral/>
      </div>
    );
  }
}

export default App;
